﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using QLSach.Models;
using PagedList;

namespace QLSach.Areas.Admin.Controllers
{
    public class HopDongController : Controller
    {
        private QLSachEntities db = new QLSachEntities();

        // GET: /Admin/HopDong/
        public ActionResult Index(int page = 1, int pageSize = 10)
        {
            var hopdong = db.HopDong.Include(h => h.NhaCungCap).OrderBy(s => s.MaHopDong).ToPagedList(page, pageSize);
            return View(hopdong);
        }
        // GET: /Admin/HopDong/Create
        public ActionResult Create()
        {
            ViewBag.MaNCC = new SelectList(db.NhaCungCap, "MaNCC", "TenNCC");
            return View();
        }

        // POST: /Admin/HopDong/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="MaHopDong,NgayBatDau,NgayKetThuc,TinhTrang,MaNCC")] HopDong hopdong)
        {
            if (ModelState.IsValid)
            {
                db.HopDong.Add(hopdong);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.MaNCC = new SelectList(db.NhaCungCap, "MaNCC", "TenNCC", hopdong.MaNCC);
            return View(hopdong);
        }

        // GET: /Admin/HopDong/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HopDong hopdong = db.HopDong.Find(id);
            if (hopdong == null)
            {
                return HttpNotFound();
            }
            ViewBag.MaNCC = new SelectList(db.NhaCungCap, "MaNCC", "TenNCC", hopdong.MaNCC);
            return View(hopdong);
        }

        // POST: /Admin/HopDong/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="MaHopDong,NgayBatDau,NgayKetThuc,TinhTrang,MaNCC")] HopDong hopdong)
        {
            if (ModelState.IsValid)
            {
                db.Entry(hopdong).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.MaNCC = new SelectList(db.NhaCungCap, "MaNCC", "TenNCC", hopdong.MaNCC);
            return View(hopdong);
        }

        // GET: /Admin/HopDong/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HopDong hopdong = db.HopDong.Find(id);
            if (hopdong == null)
            {
                return HttpNotFound();
            }
            return View(hopdong);
        }

        // POST: /Admin/HopDong/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            HopDong hopdong = db.HopDong.Find(id);
            db.HopDong.Remove(hopdong);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
