﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using QLSach.Models;
using PagedList;

namespace QLSach.Areas.Admin.Controllers
{
    public class ChiTietHopDongController : Controller
    {
        private QLSachEntities db = new QLSachEntities();

        // GET: /Admin/ChiTietHopDong/
        public ActionResult Index(int id,int page=1, int pageSize=10)
        {
            var chitiethopdong = db.ChiTietHopDong.Include(c => c.HopDong).Include(c => c.Sach).Where(c=>c.MaHopDong==id).OrderBy(s=>s.MaHopDong).ToPagedList(page, pageSize);
            return View(chitiethopdong);
        }
        // GET: /Admin/ChiTietHopDong/Create
        public ActionResult Create()
        {
            ViewBag.MaHopDong = new SelectList(db.HopDong, "MaHopDong", "MaHopDong");
            ViewBag.MaSach = new SelectList(db.Sach, "MaSach", "TenSach");
            return View();
        }

        // POST: /Admin/ChiTietHopDong/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "MaHopDong,MaSach,DonGia")] ChiTietHopDong chitiethopdong)
        {
            if (ModelState.IsValid)
            {
                db.ChiTietHopDong.Add(chitiethopdong);
                db.SaveChanges();
                return RedirectToAction("Index", new { id=chitiethopdong.MaHopDong });
            }
            ViewBag.MaHopDong = new SelectList(db.HopDong, "MaHopDong", "TinhTrang", chitiethopdong.MaHopDong);
            ViewBag.MaSach = new SelectList(db.Sach, "MaSach", "TenSach", chitiethopdong.MaSach);
            return RedirectToAction("Index", new { id = chitiethopdong.MaHopDong  });
        }

        // GET: /Admin/ChiTietHopDong/Delete/5
        public ActionResult Delete(int MaHD, int MaSach)
        {
            var result = db.ChiTietHopDong.Where(s => s.MaHopDong == MaHD);
            foreach(var item in result)
            {
                if(item.MaSach==MaSach)
                {
                    db.ChiTietHopDong.Remove(item);
                    db.SaveChanges();
                }
            }
            return Redirect("/Admin/ChiTietHopDong/Index/"+MaHD);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
